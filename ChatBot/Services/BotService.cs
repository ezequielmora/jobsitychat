﻿using System;
using System.Net.Http;
using Domain.Models;

namespace ChatBot.Services
{
    public class BotService : IBotService
    {
        public HttpClient Client { get; }

        public BotService(HttpClient _client)
        {
            Client = _client;
        }

        public Stock GetStock(string inputCode)
        {
            using (HttpResponseMessage responseReceived = Client.GetAsync($"https://stooq.com/q/l/?s={inputCode}&f=sd2t2ohlcv&h&e=csv").Result)
            using (HttpContent content = responseReceived.Content)
            {
                var serializedResponse = content.ReadAsStringAsync().Result;
                if (responseReceived.StatusCode != System.Net.HttpStatusCode.OK)
                    throw new ArgumentException(serializedResponse);
                var completeDataReceived = serializedResponse.Substring(serializedResponse.IndexOf(Environment.NewLine, StringComparison.Ordinal) + 2);
                var serializedArray = completeDataReceived.Split(',');
                return new Stock()
                {
                    Symbol = serializedArray[0].ToString(),
                    Date = Convert.ToDateTime(serializedArray[1]),
                    Time = Convert.ToDateTime(serializedArray[2]).TimeOfDay,
                    Open = float.Parse(serializedArray[3].Replace('.', ',')),
                    High = float.Parse(serializedArray[4].Replace('.', ',')),
                    Low = float.Parse(serializedArray[5].Replace('.', ',')),
                    Close = float.Parse(serializedArray[6].Replace('.', ',')),
                    Volume = float.Parse(serializedArray[7].Replace('.', ',')),
                };
            }
        }
    }
}
