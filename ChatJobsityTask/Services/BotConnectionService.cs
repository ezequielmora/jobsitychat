﻿using Domain.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;

namespace ChatJobsityTask.Services
{
    public class BotConnectionService : IBotConnectionService
    {
        private HttpClient client { get; set; }

        public BotConnectionService(HttpClient _client)
        {
            client = _client;
        }

        public ApiResponse BotDetection(string message)
        {
            try
            {
                if (message.ToLower().Contains("/stock="))
                {
                    string code = message.Replace("/stock=", "");
                    using (HttpResponseMessage response = client.GetAsync($"https://localhost:44302/api/ChatBot/GetStock?inputCode={code}").Result)
                    using (HttpContent content = response.Content)
                    {
                        string serviceResponse = content.ReadAsStringAsync().Result;
                        if (response.StatusCode != System.Net.HttpStatusCode.OK)
                            return new ApiResponse { Detected = true, IsSuccessful = false, ErrorMessage = response.StatusCode.ToString() };

                        var stock = JsonConvert.DeserializeObject<Stock>(serviceResponse);
                        return new ApiResponse { Detected = true, IsSuccessful = true, Symbol = stock.Symbol, ClosePrice = stock.Close };
                    }
                }
                return new ApiResponse { Detected = false };
            }
            catch (Exception ex)
            {
                return new ApiResponse { Detected = true, IsSuccessful = false, ErrorMessage = ex.Message };
            }
        }
    }
}
