﻿using ChatJobsityTask.Models;
using ChatJobsityTask.Services;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace ChatJobsityTask.Hubs
{
    public class ChatHub : Hub
    {
        private readonly IBotConnectionService StockBotService;

        public ChatHub(IBotConnectionService _stockBotService)
        {
            StockBotService = _stockBotService;
        }

        public async Task SendMessage(Message message)
        {
            await Clients.All.SendAsync("receiveMessage", message);
            var botResponse = StockBotService.BotDetection(message.Text);
            if (botResponse.Detected)
                if (botResponse.IsSuccessful)
                {
                    await Clients.All.SendAsync("receiveMessage", StockBotMessage($"{botResponse.Symbol} QUOTE IS $ {botResponse.ClosePrice} PER SHARE"));
                }
                else
                {
                    await Clients.All.SendAsync("receiveMessage", StockBotMessage($"Sorry we had an error. { botResponse.ErrorMessage }; try with another code"));
                }
        }

        /// <summary>
        /// Create a Bot message
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        internal Message StockBotMessage(string text)
        {
            return new Message
            {
                UserName = "StockBot",
                Text = text,
                When = DateTime.Now
            };
        }

    }
}
