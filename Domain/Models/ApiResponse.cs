﻿namespace Domain.Models
{
    public class ApiResponse
    {
        public bool IsSuccessful { get; set; }
        public bool Detected { get; set; }
        public string ErrorMessage { get; set; }
        public string Symbol { get; set; }
        public float ClosePrice { get; set; }
    }
}
